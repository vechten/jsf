package pl.jazapp.app;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@SessionScoped
@Named
public class UserContext implements Serializable {
    private static final long serialVersionUID = 2l;
    private boolean isLogged;
    private String welcomeMessage;

    public String getWelcomeMessage() {
        return welcomeMessage;
    }

    public void setWelcomeMessage(String welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
    }

    public boolean isLogged() {
        return isLogged;
    }

    public void logIn() {
        isLogged = true;
    }

    public void logOut() {
        isLogged = false;
    }
}
