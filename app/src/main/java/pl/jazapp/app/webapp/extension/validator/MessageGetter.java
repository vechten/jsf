package pl.jazapp.app.webapp.extension.validator;

import javax.faces.context.FacesContext;
import java.util.PropertyResourceBundle;

public class MessageGetter {

    public static String getMsg(String key){
        FacesContext context = FacesContext.getCurrentInstance();
        var msg = context.getApplication().evaluateExpressionGet(
                context,"#{msg}", PropertyResourceBundle.class
        );
        return msg.getString(key);
    }
}

