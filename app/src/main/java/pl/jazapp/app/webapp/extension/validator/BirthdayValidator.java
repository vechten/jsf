package pl.jazapp.app.webapp.extension.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Pattern;

@FacesValidator
public class BirthdayValidator implements Validator<String> {
    private final Pattern birthdayValidator = Pattern.compile("^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\\d\\d$");

    @Override
    public void validate(FacesContext context, UIComponent component, String value) throws ValidatorException {
        if(!birthdayValidator.matcher(value).matches()){
            var message = MessageGetter.getMsg("pl.jazapp.app.webapp.extension.validator.BirthdayValidator");
            throw new ValidatorException(new FacesMessage(message));        }
    }
}
