package pl.jazapp.app.webapp.register;

import pl.jazapp.app.webapp.user.User;
import pl.jazapp.app.webapp.user.UserService;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@RequestScoped
@Named
public class RegisterController {
    @Inject
    private UserService userService;

    public String register(RegisterRequest registerRequest) {
        FacesContext context = FacesContext.getCurrentInstance();

        if (!registerRequest.getPassword().equals(registerRequest.getPasswordCheck())) {
            context.addMessage(null, new FacesMessage("Passwords do not match."));
            return null;
        } else if (userService.userExists(registerRequest.getUsername())) {
            context.addMessage(null, new FacesMessage("Username is already taken."));
            return null;

        } else {
            userService.addUser(registerRequest.getUsername(), new User(registerRequest.getUsername(), registerRequest.getPassword(),
                    registerRequest.getFirstName().substring(0, 1).toUpperCase() + registerRequest.getFirstName().substring(1),
                    registerRequest.getLastName().substring(0, 1).toUpperCase() + registerRequest.getLastName().substring(1),
                    registerRequest.getBirthday(), registerRequest.getEmail()));
            return "/login.html?faces-redirect=true";
        }
    }

}
