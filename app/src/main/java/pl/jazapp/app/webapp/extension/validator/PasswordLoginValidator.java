package pl.jazapp.app.webapp.extension.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Pattern;
@FacesValidator
public class PasswordLoginValidator implements Validator<String> {
    private final Pattern onlySmallLettersPattern = Pattern.compile("^.{1,25}$");

    @Override
    public void validate(FacesContext context, UIComponent component, String value) throws ValidatorException {
        if (!onlySmallLettersPattern.matcher(value).matches()) {
            var message = MessageGetter.getMsg("pl.jazapp.app.webapp.extension.validator.UsernameValidator.MAXIMUM");
            throw new ValidatorException(new FacesMessage(message));
        }

    }
}
