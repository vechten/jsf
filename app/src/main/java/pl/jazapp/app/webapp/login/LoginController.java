package pl.jazapp.app.webapp.login;

import pl.jazapp.app.UserContext;
import pl.jazapp.app.webapp.user.UserService;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@RequestScoped
@Named
public class LoginController {
    @Inject
    private UserContext userContext;
    @Inject
    private UserService userService;

    public String login(LoginRequest loginRequest) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (userService.isUsernameAndPasswordCorrect(loginRequest.getUsername(), loginRequest.getPassword())) {
            var user = userService.getUser(loginRequest.getUsername());
            userContext.logIn();
            userContext.setWelcomeMessage(user.getFullName());
            return "/index.html?faces-redirect=true";
        } else {
            context.addMessage(null, new FacesMessage("Wrong username or password"));
            return null;
        }
    }


}
