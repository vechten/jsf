package pl.jazapp.app.webapp.logout;


import pl.jazapp.app.UserContext;

import javax.enterprise.context.RequestScoped;

import javax.inject.Inject;
import javax.inject.Named;

@RequestScoped
@Named
public class LogoutController {
    @Inject
    UserContext userContext;

    public String logout() {
        userContext.logOut();
        return "/login.html?faces-redirect=true";
    }

}
