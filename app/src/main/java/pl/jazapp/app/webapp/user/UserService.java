package pl.jazapp.app.webapp.user;


import javax.enterprise.context.ApplicationScoped;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ApplicationScoped
public class UserService {
    private Map<String, User> users = new ConcurrentHashMap<>();

    public Boolean userExists(String username) {
        return users.containsKey(username);
    }

    public User getUser(String username) {
        return users.get(username);
    }

    public void addUser(String username, User user) {
        users.put(username, user);
    }

    public Boolean isUsernameAndPasswordCorrect(String username, String password) {
        try {
            var user = users.get(username);
            return user.getUsername().equals(username) && user.getPassword().equals(password);
        } catch (NullPointerException e) {
            return false;
        }
    }
}
